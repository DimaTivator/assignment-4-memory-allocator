#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#ifndef TESTS_NUM
#define TESTS_NUM 5
#endif

#define HEAP_SIZE 4096

bool default_malloc_success(void) {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "Improper heap initialization");

    debug_heap(stderr, heap);

    void* big_block = _malloc(1024);
    void* small_block = _malloc(3);

    struct block_header* big_block_header = block_get_header(big_block);
    struct block_header* small_block_header = block_get_header(small_block);

    assert(big_block && "Allocation failed for big block");
    assert(small_block && "Allocation failed for small block");
    assert(!big_block_header->is_free && "Big block is not marked as allocated");
    assert(!small_block_header->is_free && "Small block is not marked as allocated");
    assert(big_block_header->capacity.bytes >= 1024 && "Big block capacity is less than required");
    assert(small_block_header->capacity.bytes >= 3 && "Small block capacity is less than required");

    heap_term();

    return true;
}

bool free_one_from_many(void) {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "Improper heap initialization");

    debug_heap(stderr, heap);

    void* block1 = _malloc(48);
    void* block2 = _malloc(96);
    void* block3 = _malloc(256);

    assert(block1 && "Allocation failed for block1");
    assert(block2 && "Allocation failed for block2");
    assert(block3 && "Allocation failed for block3");

    struct block_header* block1_header = block_get_header(block1);
    struct block_header* block2_header = block_get_header(block2);
    struct block_header* block3_header = block_get_header(block3);

    _free(block2);

    debug_heap(stderr, heap);

    assert(!block1_header->is_free && "Block1 is marked as free");
    assert(block2_header->is_free && "Block2 is not marked as free");
    assert(!block3_header->is_free && "Block3 is marked as free");
    assert(block1_header->next == block2_header && "Block1's next is not pointing to Block2");
    assert(block2_header->next == block3_header && "Block2's next is not pointing to Block3");

    heap_term();

    return true;
}

bool free_two_from_many(void) {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "Improper heap initialization");

    debug_heap(stderr, heap);

    void* block1 = _malloc(48);
    void* block2 = _malloc(96);
    void* block3 = _malloc(256);

    assert(block1 && "Allocation failed for block1");
    assert(block2 && "Allocation failed for block2");
    assert(block3 && "Allocation failed for block3");

    struct block_header* block1_header = block_get_header(block1);
    struct block_header* block2_header = block_get_header(block2);
    struct block_header* block3_header = block_get_header(block3);

    _free(block2);
    debug_heap(stderr, heap);

    _free(block3);
    debug_heap(stderr, heap);

    assert(!block1_header->is_free && "Block1 is marked as free");
    assert(block2_header->is_free && "Block2 is not marked as free");
    assert(block3_header->is_free && "Block3 is not marked as free");
    assert(block1_header->next == block2_header && "Block1's next is not pointing to Block2");
    assert(block2_header->next == block3_header && "Block2's next is not pointing to Block3");

    heap_term();

    return true;
}

bool no_memory_easy_expansion(void) {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "Improper heap initialization");

    debug_heap(stderr, heap);

    const size_t SIZE = 2 * HEAP_SIZE;

    void* block1 = _malloc(HEAP_SIZE / 2);
    assert(block1 && "Allocation failed for block1");

    struct block_header* block1_header = block_get_header(block1);

    size_t block1_init_capacity = block1_header->capacity.bytes;

    debug_heap(stderr, heap);

    void* block2 = _malloc(SIZE);
    assert(block2 && "Allocation failed for block2");

    struct block_header* block2_header = block_get_header(block2);

    size_t expanded_heap_size = ((struct region*) heap)->size;

    assert(expanded_heap_size > HEAP_SIZE && "Heap did not expand as expected");
    assert(block1_header->capacity.bytes >= block1_init_capacity && "Block1 capacity reduced after expansion");
    assert(block2_header->capacity.bytes >= SIZE && "Block2 capacity is less than required");

    heap_term();

    return true;
}

bool no_memory_hard_expansion(void) {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap && "Improper heap initialization");

    debug_heap(stderr, heap);

    void* garbage = map_pages(heap + 100, HEAP_SIZE, MAP_FIXED);
    assert(garbage && "Garbage mapping failed");

    debug_heap(stderr, heap);

    void* block1 = _malloc(HEAP_SIZE / 2);
    assert(block1 && "Allocation failed for block1");

    debug_heap(stderr, heap);

    struct block_header* block1_header = block_get_header(block1);
    size_t block1_init_capacity = block1_header->capacity.bytes;

    // there'll be not enough memory for this block
    void* block2 = _malloc(HEAP_SIZE);
    assert(block2 && "Allocation failed for block2");

    struct block_header* block2_header = block_get_header(block2);

    size_t expanded_heap_size = ((struct region*) heap)->size;

    assert(expanded_heap_size > HEAP_SIZE && "Heap did not expand as expected");
    assert(block1_header->capacity.bytes >= block1_init_capacity && "Block1 capacity reduced after expansion");
    assert(block2_header->capacity.bytes >= HEAP_SIZE && "Block2 capacity is less than required");

    heap_term();

    return true;
}

int main() {
    bool (*tests[TESTS_NUM]) () = {
            default_malloc_success
            ,free_one_from_many
            ,free_two_from_many
            ,no_memory_easy_expansion
            ,no_memory_hard_expansion
    };

    for (int i = 0; i < TESTS_NUM; i++) {
        if (tests[i]()) {
            printf("TEST %d - SUCCESS\n", i + 1);
        }
    }
}
